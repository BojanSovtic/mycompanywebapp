package com.engineering.mycompanywebapp.view;

import java.util.HashMap;
import java.util.Map;

import com.engineering.mycompanywebapp.util.Params;

public class ViewResolver {

	Map<String, String> maps;
	
	public ViewResolver() {
		maps = new HashMap<String, String>();
		maps.put(Params.VIEW_LOGIN,Params.PAGE_LOGIN);
		maps.put(Params.VIEW_HOME, Params.PAGE_HOME);
		maps.put(Params.VIEW_DEFAULT_ERROR, Params.PAGE_DEFAULT_ERROR);
		maps.put(Params.VIEW_CITY_ALL, Params.PAGE_CITY_ALL);
		maps.put(Params.VIEW_MANUFACTURER_ADD, Params.PAGE_MANUFACTURER_ADD);
		maps.put(Params.VIEW_MANUFACTURER_ALL, Params.PAGE_MANUFACTURER_ALL);
		maps.put(Params.VIEW_CITY_ADD, Params.PAGE_CITY_ADD);
	}
	
	public String resolve(String key) {
		return maps.get(key);
	}
}
