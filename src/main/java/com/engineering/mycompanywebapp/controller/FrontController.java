package com.engineering.mycompanywebapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.engineering.mycompanywebapp.view.ViewResolver;

/**
 * Servlet implementation class FrontController
 */
@WebServlet(urlPatterns = {"/application/*"})
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ApplicationController applicationController;
    private ViewResolver viewResolver;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String view = applicationController.processRequest(request);
		System.out.println(view);
		String page = viewResolver.resolve(view);
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String view = applicationController.processRequest(request);
		String page = viewResolver.resolve(view);
		request.getRequestDispatcher(page).forward(request, response);
	}

	@Override
	public void init() throws ServletException {
		super.init();
		applicationController = new ApplicationController();
		viewResolver = new ViewResolver();
		System.out.println("ApplicationController is created");
	}
}
