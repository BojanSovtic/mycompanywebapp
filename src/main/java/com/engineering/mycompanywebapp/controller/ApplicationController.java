package com.engineering.mycompanywebapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.engineering.mycompanywebapp.action.impl.LoginViewAction;
import com.engineering.mycompanywebapp.action.impl.LogoutAction;
import com.engineering.mycompanywebapp.action.impl.ManufacturerAddAction;
import com.engineering.mycompanywebapp.action.impl.ManufacturerAllAction;
import com.engineering.mycompanywebapp.action.impl.ManufacturerSaveAction;
import com.engineering.mycompanywebapp.util.Params;
import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.action.impl.CityAction;
import com.engineering.mycompanywebapp.action.impl.CityAddAction;
import com.engineering.mycompanywebapp.action.impl.CitySaveAction;
import com.engineering.mycompanywebapp.action.impl.LoginAction;

public class ApplicationController {

	public String processRequest(HttpServletRequest request) {
//		String page = "/WEB-INF/pages/error/default-error-page.jsp";
		String view = Params.VIEW_LOGIN;
		String path = request.getPathInfo();
		System.out.println("PathInfo: " + path);
		
		// proveri da li je korisnik prijavljen na sistem
		if (!checkAuthenticate(request, path))
			return view;
		
		view = Params.VIEW_DEFAULT_ERROR;
		
		AbstractAction action;

		if(path.equalsIgnoreCase(Params.PATH_LOGIN)) {
			action = new LoginAction();
			return action.execute(request);
		}else if(path.equalsIgnoreCase(Params.PATH_VIEW_LOGIN)) {
			action = new LoginViewAction();
			return action.execute(request);
		} else if(path.equalsIgnoreCase(Params.PATH_LOGOUT)) {
			action = new LogoutAction();
			return action.execute(request);
		} else if (path.equalsIgnoreCase(Params.PATH_CITY_ALL)) {
			action = new CityAction();
			return action.execute(request);
		} else if (path.equalsIgnoreCase(Params.PATH_MANUFACTURER_ADD)) {
			action = new ManufacturerAddAction();
			return action.execute(request);
		} else if(path.equalsIgnoreCase(Params.PATH_MANUFACTURER_SAVE)) {
			action = new ManufacturerSaveAction();
			return action.execute(request);
		} else if(path.equalsIgnoreCase(Params.PATH_MANUFACTURER_ALL)) {
			action = new ManufacturerAllAction();
			return action.execute(request);
		} else if(path.equalsIgnoreCase(Params.PATH_CITY_ADD)) {
			action = new CityAddAction();
			return action.execute(request);
		} else if (path.equalsIgnoreCase(Params.PATH_CITY_SAVE)) {
			action = new CitySaveAction();
			return action.execute(request);
		}
		
		return view;
	}

	private boolean checkAuthenticate(HttpServletRequest request, String path) {
		if (path.equalsIgnoreCase(Params.PATH_VIEW_LOGIN) || path.equalsIgnoreCase(Params.PATH_LOGIN))
			return true;
		
		HttpSession session = request.getSession();
		
		return session.getAttribute("loginUser") != null;
	}
}
