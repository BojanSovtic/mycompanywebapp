package com.engineering.mycompanywebapp.listener;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.engineering.mycompanywebapp.model.City;
import com.engineering.mycompanywebapp.model.Manufacturer;
import com.engineering.mycompanywebapp.model.User;

/**
 * Application Lifecycle Listener implementation class StartupListener
 *
 */
@WebListener("Startup")
public class StartupListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public StartupListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
         List<User> users = new ArrayList<User>() {
        	 {
        		 add(new User(1L, "admin", "admin"));
        		 add(new User(2L, "user", "user"));
        	 }       	 
         };
         sce.getServletContext().setAttribute("users", users);
         
         List<City> cities = new ArrayList<City>() {
        	 {
        		 add(new City(1L, 11000L, "Beograd"));
        		 add(new City(2L, 11400L, "Mladenovac"));
        		 add(new City(3L, 18000L, "Nis"));
        	 }
         };
         
         sce.getServletContext().setAttribute("cities", cities);
         
         sce.getServletContext().setAttribute("manufacturers", new ArrayList<Manufacturer>());
    }
	
}
