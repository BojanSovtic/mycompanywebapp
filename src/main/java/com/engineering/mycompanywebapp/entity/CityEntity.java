package com.engineering.mycompanywebapp.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@Table(name = "city")
@NamedQuery(name = "CityEntity.findAll", query = "SELECT c FROM CityEntity c")
public class CityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long number;
	private String name;

	public CityEntity() {
	}

	public CityEntity(Long number, String name) {
		this.number = number;
		this.name = name;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityEntity [number=" + number + ", name=" + name + "]";
	}

}