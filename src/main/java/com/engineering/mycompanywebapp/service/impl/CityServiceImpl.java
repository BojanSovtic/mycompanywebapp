package com.engineering.mycompanywebapp.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.engineering.mycompanywebapp.dao.Dao;
import com.engineering.mycompanywebapp.dao.impl.JpaCityDao;
import com.engineering.mycompanywebapp.entity.CityEntity;
import com.engineering.mycompanywebapp.model.City;
import com.engineering.mycompanywebapp.service.CityService;

public class CityServiceImpl implements CityService {
	private Dao<CityEntity> cityDao;
	private EntityManager em;
	
	public CityServiceImpl() {
		em = Persistence.createEntityManagerFactory("mycompanywebapp").createEntityManager();
		cityDao = new JpaCityDao(em);
	}

	@Override
	public void save(City city) {
		CityEntity cityEntity = new CityEntity();

		cityEntity.setName(city.getName());
		cityEntity.setNumber(city.getCode());
		
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			cityDao.save(cityEntity);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} 

	}

}
