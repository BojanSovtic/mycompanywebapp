package com.engineering.mycompanywebapp.service;

import com.engineering.mycompanywebapp.model.City;

public interface CityService {
	
	void save(City city);
	
}
