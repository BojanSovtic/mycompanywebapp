package com.engineering.mycompanywebapp.action.impl;

import javax.servlet.http.HttpServletRequest;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.util.Params;

public class CityAddAction extends AbstractAction {

	@Override
	public String execute(HttpServletRequest request) {
		
		return Params.VIEW_CITY_ADD;
	}

}
