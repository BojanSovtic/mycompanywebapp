package com.engineering.mycompanywebapp.action.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.util.Params;

public class LogoutAction extends AbstractAction{

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("loginUser");
		session.invalidate();
		session = request.getSession(false);
		return Params.VIEW_LOGIN;
	}

}
