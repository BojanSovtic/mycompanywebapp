package com.engineering.mycompanywebapp.action.impl;
import javax.servlet.http.HttpServletRequest;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.util.Params;

public class LoginViewAction extends AbstractAction {
	
	@Override
	public String execute(HttpServletRequest request) {
		
		return Params.VIEW_LOGIN;
	}

}
