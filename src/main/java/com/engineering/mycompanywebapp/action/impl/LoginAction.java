package com.engineering.mycompanywebapp.action.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.model.User;
import com.engineering.mycompanywebapp.util.Params;

public class LoginAction extends AbstractAction {

	@Override
	public String execute(HttpServletRequest request) {
		
		//TODO 
		User user = login(request);
		
		if(user != null) {
			HttpSession session = request.getSession(true);
			session.setAttribute("loginUser", user);
			return Params.VIEW_HOME;	
		}else {
			request.setAttribute("message", "User doesn't exist");
//			return "/WEB-INF/pages/login.jsp";
			return Params.VIEW_LOGIN;
		}
	}
	
	
	private User login(HttpServletRequest request) {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		List<User> users = ((List<User>) request.getServletContext().getAttribute("users"));
		
		for(User u: users) {
			if(username.equals(u.getUsername()) && password.equals(u.getPassword())) {
				return u;
			}
		}
		
		return null;
	}
}
