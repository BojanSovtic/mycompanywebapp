package com.engineering.mycompanywebapp.action.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.model.City;
import com.engineering.mycompanywebapp.model.Manufacturer;
import com.engineering.mycompanywebapp.util.Params;

public class ManufacturerSaveAction extends AbstractAction{

	@Override
	public String execute(HttpServletRequest request) {
		Manufacturer manufacturer = createManufacturer(request);
		
		if(manufacturer != null) {			
			return Params.VIEW_HOME;
		}else {
			request.setAttribute("message", "Greska kod dodavanja");
			return Params.VIEW_MANUFACTURER_ADD;
		}
		
	}

	private Manufacturer createManufacturer(HttpServletRequest request) {
		try {
			Long id = Long.parseLong(request.getParameter("id"));
			String name = request.getParameter("name");
			Long cityId = Long.parseLong(request.getParameter("city"));
			City city = getCity(cityId, request);
			
			if(city != null) {
				Manufacturer manufacturer = new Manufacturer(id, name, city);
				((List<Manufacturer>) request.getServletContext().getAttribute("manufacturers"))
					.add(manufacturer);
				return manufacturer;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
 
	private City getCity(Long id, HttpServletRequest request) {
		List<City> cities = (List<City>) request.getServletContext().getAttribute("cities");
		for(City c: cities) {
			if(c.getId().equals(id)) {
				return c;
			}
		}		
		return null;
	}
	
}
