package com.engineering.mycompanywebapp.action.impl;

import javax.servlet.http.HttpServletRequest;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.model.City;
import com.engineering.mycompanywebapp.service.CityService;
import com.engineering.mycompanywebapp.service.impl.CityServiceImpl;
import com.engineering.mycompanywebapp.util.Params;

public class CitySaveAction extends AbstractAction {
	private CityService cityService;

	public CitySaveAction() {
		cityService = new CityServiceImpl();
	}

	@Override
	public String execute(HttpServletRequest request) {
		City city = createCity(request);
		
		if (city != null) {
			cityService.save(city);
			return Params.VIEW_HOME;
		} else {
			request.setAttribute("cityNumber", request.getParameter("number"));
			request.setAttribute("cityName", request.getParameter("name"));
			request.setAttribute("message", "Greska kod dodavanja");
			return Params.VIEW_CITY_ADD;
		}
	}

	private City createCity(HttpServletRequest request) {
		try {
			Long number = Long.parseLong(request.getParameter("number"));
			String name = request.getParameter("name");
			
			City city = new City();
			city.setCode(number);
			city.setName(name);
			
			return city;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
