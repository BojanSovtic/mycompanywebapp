package com.engineering.mycompanywebapp.action.impl;

import javax.servlet.http.HttpServletRequest;

import com.engineering.mycompanywebapp.action.AbstractAction;
import com.engineering.mycompanywebapp.util.Params;

public class CityAction extends AbstractAction {

	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute("cityList", request.getServletContext().getAttribute("cities"));
		return Params.VIEW_CITY_ALL;
	}

}
