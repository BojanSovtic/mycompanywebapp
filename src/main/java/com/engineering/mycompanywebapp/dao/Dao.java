package com.engineering.mycompanywebapp.dao;

public interface Dao<T> {
	
	void save(T object);
	
}
