package com.engineering.mycompanywebapp.dao.impl;

import javax.persistence.EntityManager;

import com.engineering.mycompanywebapp.dao.Dao;
import com.engineering.mycompanywebapp.entity.CityEntity;

public class JpaCityDao implements Dao<CityEntity> {
	private final EntityManager entityManager;

	public JpaCityDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public void save(CityEntity city) {
		entityManager.persist(city);
	}

}
