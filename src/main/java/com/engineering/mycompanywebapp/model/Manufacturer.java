package com.engineering.mycompanywebapp.model;

public class Manufacturer {
	
	private Long id;
	private String name;
	private City city;
	
	public Manufacturer() {
		// TODO Auto-generated constructor stub
	}

	public Manufacturer(Long id, String name, City city) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	

}
