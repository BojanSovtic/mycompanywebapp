package com.engineering.mycompanywebapp.model;

public class City {
	
	private Long id;
	private Long code;
	private String name;
	
	public City() {
		// TODO Auto-generated constructor stub
	}

	public City(Long id, Long code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
