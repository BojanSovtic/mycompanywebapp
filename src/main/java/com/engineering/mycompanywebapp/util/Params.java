package com.engineering.mycompanywebapp.util;

public interface Params {

	public static final String PATH_VIEW_LOGIN = "/login-view";
	public static final String VIEW_LOGIN = "view-login";
	public static final String PAGE_LOGIN = "/WEB-INF/pages/login.jsp";
	
	public static final String PATH_LOGIN = "/login";

	public static final String VIEW_HOME = "view-home";
	public static final String PAGE_HOME = "/WEB-INF/pages/home.jsp";
	
	public static final String VIEW_DEFAULT_ERROR = "view-error-default";
	public static final String PAGE_DEFAULT_ERROR = "/WEB-INF/pages/error/default-error-page.jsp";

	public static final String PATH_LOGOUT = "/logout";
	
	public static final String PATH_CITY_ALL = "/city/all";
	public static final String VIEW_CITY_ALL = "city-all";
	public static final String PAGE_CITY_ALL = "/WEB-INF/pages/city/city-list.jsp";
	
	public static final String PATH_MANUFACTURER_ADD = "/manufacturer/add";
	public static final String VIEW_MANUFACTURER_ADD = "manufacturer-add";
	public static final String PAGE_MANUFACTURER_ADD = "/WEB-INF/pages/manufacturer/manufacturer-add.jsp";

	public static final String PATH_MANUFACTURER_SAVE = "/manufacturer/save";
	
	public static final String PATH_MANUFACTURER_ALL = "/manufacturer/all";
	public static final String VIEW_MANUFACTURER_ALL = "manufacturer-all";
	public static final String PAGE_MANUFACTURER_ALL = "/WEB-INF/pages/manufacturer/manufacturer-list.jsp";
	
	public static final String PATH_CITY_ADD = "/city/add";
	public static final String VIEW_CITY_ADD = "city-add";
	public static final String PAGE_CITY_ADD = "/WEB-INF/pages/city/city-add.jsp";

	public static final String PATH_CITY_SAVE = "/city/save";
}
