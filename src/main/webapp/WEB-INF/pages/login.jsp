<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login page</title>
<!-- link href="<c:url value="/resources/styles/style.css"/>" type="text/css" rel="stylesheet" -->
<link href="<c:url value="/resources/styles/login.css"/>" type="text/css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Poppins&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
	integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
	crossorigin="anonymous" />

</head>
<body>

<div class="flex-wrapper">
		<div class="login-box">
			<h1>Welcome!</h1>
			<form action="/mycompanywebapp/application/login" method="POST">
				<span class="error-msg">${message}</span>
				<div <c:if test="${not empty message}"> class="error" </c:if>  >
					<i class="fas fa-user"></i> <input type="text"
						placeholder="username" name="username">
				</div>
				<div <c:if test="${not empty message}"> class="error" </c:if> >
					<i class="fas fa-lock"></i> <input type="password"
						placeholder="password" name="password">
				</div>
				<input type="submit" name="Submit">
			</form>
		</div>
	</div>
</body>
</html>