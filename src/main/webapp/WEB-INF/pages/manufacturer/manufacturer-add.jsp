<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create new manufacturer</title>
<link href="<c:url value="/resources/styles/home.css"/>" type="text/css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
  integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
</head>
<body>
  <header>
    <jsp:include page="/WEB-INF/template/login-user.jsp" flush="true" />
    <jsp:include page="/WEB-INF/template/navigation.jsp" flush="true" />
  </header>
  <main>
    <div>
      <h1>Add new manufacturer</h1>
      <form action="/mycompanywebapp/application/manufacturer/save" method="post">
        <p>${message}</p>
        <div>
          <label>ID</label> <input name="id">
        </div>
        <div>
          <label>Name</label> <input name="name">
        </div>
        <div>
          <label>City</label> <select name="city">
            <option value="0">-- Izaberi grad --</option>
            <c:forEach var="city" items="${cities}">
              <option value="${city.id}">${city.name}</option>
            </c:forEach>
          </select>
        </div>
        <div>
          <label></label> <input type="submit" value="Submit">
        </div>
      </form>
    </div>
  </main>
  <footer> Copyright 2020. </footer>
</body>
</html>