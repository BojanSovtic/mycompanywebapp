<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<link href="<c:url value="/resources/styles/home.css"/>" type="text/css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Poppins&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
	integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
	crossorigin="anonymous" />
</head>
<body>
	<header>
		<jsp:include page="/WEB-INF/template/login-user.jsp" flush="true" />
		<jsp:include page="/WEB-INF/template/navigation.jsp" flush="true" />
	</header>
	<main>
		<h1>HOME page</h1>
		<article>
			<div>
				<i class="fas fa-city fa-5x"></i>
				<h3>Explore all cities</h3>
				<p>Follow this link if you want to explore all available cities</p>
				<a href="<c:url value = "/application/city/all"/>">Cities</a>
			</div>
			<div>
				<i class="fas fa-industry fa-5x"></i>
				<h3>Explore all manufacturers</h3>
				<p>Follow this link if you want to explore manufacturers</p>
				<a href="<c:url value = "/application/manufacturer/all"/>">Manufacturers</a>
			</div>
			<div>
				<i class="fas fa-industry fa-5x"></i> <i
					class="far fa-plus-square fa-3x"></i>
				<h3>Add new manufacturer</h3>
				<p>Follow this link if you want to add new manufacturer</p>
				<a href="<c:url value = "/application/manufacturer/add"/>">Add
					manufacturer</a>
			</div>
		</article>

		<article>
			<figure class="right-slide-in">
				<img
					src="https://img.freepik.com/free-vector/two-business-partners-handshaking_74855-6685.jpg?size=626&ext=jpg" />
			</figure>
			<div class="left-slide-in">
				<h2>Lorem ipsum</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					Donec pretium malesuada enim, ac luctus sem. Nullam lorem purus,
					ultricies a metus a, aliquam placerat enim. Aliquam a ultricies
					diam, eu convallis elit. Cras libero arcu, pharetra eu nisi id,
					ultrices efficitur ligula. Aenean vel sollicitudin metus. Maecenas
					non nunc purus. Suspendisse sit amet molestie tellus, sit amet
					malesuada erat. Duis bibendum nisi vitae porttitor placerat. Nullam
					in vulputate ipsum.</p>
			</div>
		</article>


	</main>
	<footer> Copyright 2020. </footer>
</body>
</html>