<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City list</title>
<link href="<c:url value="/resources/styles/home.css"/>" type="text/css"
	rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Poppins&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
	integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w=="
	crossorigin="anonymous" />
</head>
<body>
	<header>
		<jsp:include page="/WEB-INF/template/navigation.jsp" flush="true"></jsp:include>
		<jsp:include page="/WEB-INF/template/login-user.jsp" flush="true"></jsp:include>
	</header>
	<main>
		<div>
			<h1>All cities</h1>

			<p>${message}</p>
			<table>
				<thead>
					<tr>
						<th>Id</th>
						<th>Code</th>
						<th>Name</th>
						<th>Details</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="city" items="${applicationScope.cities}">
						<tr>
							<td>${city.id}</td>
							<td>${city.code}</td>
							<td>${city.name}</td>
							<td><a href="/mywebapp/city/info?id=${city.id}">details</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div></div>
<h1>All cities</h1>
			<table>
			<thead>
				<tr>
					<th>Id</th>
					<th>Code</th>
					<th>Name</th>
					<th>Details</th>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="city" items="${requestScope.cityList}">
					<tr>
						<td>${city.id}</td>
						<td>${city.code}</td>
						<td>${city.name}</td>
						<td><a href="/mywebapp/city/info?id=${city.id}">details</a></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</main>
	<footer> Copyright 2020. </footer>
</body>
</html>