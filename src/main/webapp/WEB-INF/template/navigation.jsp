<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<div>
	<nav>
		<a href="<c:url value = "/application/city/all"/>">View all cities</a>
		<a href="<c:url value = "/application/manufacturer/all"/>">View
			all manufacturer</a> 
      <a
			href="<c:url value = "/application/manufacturer/add"/>">Add new
			manufacturer</a>
      <a href="<c:url value = "/application/city/add"/>">Add new city</a>
	</nav>
</div>