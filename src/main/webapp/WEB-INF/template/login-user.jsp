<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<div id="user">
	<label>Login user: </label>
	<p>${loginUser.username}</p>
	<a href = "<c:url value = "/application/logout"/>">Logout</a>
</div>